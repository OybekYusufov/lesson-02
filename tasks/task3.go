package main

import "fmt"

func main() {
	DisplayNumberInReverseOrderWithDefer()
}

func DisplayNumberInReverseOrderWithDefer() {
	var j int32=99
	for i := 0; i < 100; i++ {
		fmt.Printf("%d \n",j)
		j--
	}
}
