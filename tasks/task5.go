package main

import (
	"fmt"
)

func main() {
	n := 123045
	// Read n from input
	DisplayMinimumNumberFunction(n)
}

// https://www.hackerrank.com/contests/w30/challenges/find-the-minimum-number
func DisplayMinimumNumberFunction(n int) {
	var min int=n%10
	n=n/10
	for ; n > 0; {
		var q int = n%10
		if min>q{
			min=q
		}
		n=n/10
	}
	fmt.Println(min)
}
