package main

import (
	"fmt"
	"math"
)

func main() {
	fmt.Println(MySquareRoot(25, 0.2))
}

func MySquareRoot(num, precision float64) (result float64) {
	

	var c float64=0
	var o float64=num/2
	var t float64=1
	var a float64=1
	for ;true;{
		t=(c+o)/2
		a=math.Abs(num-(t*t))
		fmt.Println(a)
		if a<precision || t==0 {
			break
		}
		if t*t>=num{
			o=t
		}else{
			c=t
		}
//		a=t
	}
	return t
}
